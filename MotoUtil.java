public class MotoUtil {

    /*Deberá realizar un informe a partir de las motos introducidas, del siguiente tipo:

        La moto más cara de todas es la Honda Goldwing (30.000 eur)
        La moto más potente de todas es la Ducati Multistrada (160 cv)
        El precio conjunto de las motos es de 67.000 eur. 
    */
    public static void informe(Moto[] motos) {
        Double precioMasCaro = 0.0;
        String marcaCara = "";
        String modelCaro = "";
        int potencia = 0;
        Double precioCaro = 0.0;

        //para la moto mas potente
        int masPotente = 0;
        String marcaPotente = "";
        String modeloPotente = "";

        //Variables para el precio conjunto
        Double precioTotal = 0.0;

        for(int i = 0; i<motos.length; i++){
            
            Moto mim = motos[i];
            
            if(precioMasCaro < mim.getPrecio()){
                marcaCara = mim.getMarca();
                modelCaro = mim.getModelo();
                precioCaro = mim.getPrecio();
            }

            if(masPotente < mim.getPotencia()){
                marcaPotente = mim.getMarca();
                modeloPotente = mim.getModelo();
                masPotente = mim.getPotencia();
            }

            precioTotal += mim.getPrecio();
        }

        System.out.printf("La moto mas cara de todas es la %s %s (%.2f eur) \n",marcaCara,modelCaro,precioCaro);
        System.out.printf("La moto mas potente de todas es la %s %s (%d cv) \n",marcaPotente,modeloPotente,masPotente);
        System.out.printf("El precio conjunto de las motos es de %.2f eur.", precioTotal);
    }


    /* Deberá escribir en pantalla la comparación de dos motos del siguiente modo:

        La Honda CBR1000 tiene 40cv más que la BMW R1200GS
        La BMW R1200GS es 2000eur más cara que la Honda CBR1000
    */
    public static void compara(Moto moto1, Moto moto2){
        int potenciaRestante = 0;
        Double precioRestante = 0.0;
        if( moto1.getPotencia() < moto2.getPotencia()){
            potenciaRestante = moto2.getPotencia() - moto1.getPotencia();
            System.out.printf("La %s %s tiene %d cv mas que la %s %s",moto2.getMarca(),moto2.getModelo(),potenciaRestante,moto1.getMarca(),moto1.getModelo());
        } else {
            potenciaRestante = moto1.getPotencia() - moto2.getPotencia();
            System.out.printf("La %s %s tiene %d cv mas que la %s %s",moto1.getMarca(),moto1.getModelo(),potenciaRestante,moto2.getMarca(),moto2.getModelo());
        }

        if(moto1.getPrecio() < moto2.getPrecio()){
            precioRestante = moto2.getPrecio() - moto1.getPrecio();
            System.out.printf("La %s %s es %.2f eur mas cara que la %s %s",moto2.getMarca(),moto2.getModelo(),precioRestante,moto1.getMarca(),moto1.getModelo());
        } else {
            precioRestante = moto1.getPrecio() - moto2.getPrecio();
            System.out.printf("La %s %s es %.2f eur mas cara que la %s %s", moto1.getMarca(), moto1.getModelo(), precioRestante, moto2.getMarca(), moto2.getModelo());
        }
    }

    /*
    public static Moto masPotente(Moto[] motos){
        return motos;
    }

    public static Moto masCara(Moto[] motos){

    }
    */
}