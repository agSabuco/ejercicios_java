public class TestPerson{

    public static void main(String[] args) {
        //Genero objetos personas
        Persona p1 = new Persona("Ana", "ana@gmail.com", 26, 1234);
        Persona p2 = new Persona("Rub\u00e9n", "ruben@gmail.com", 24, 4321);

        PersonaController metodos = new PersonaController();

        //Llamada de metodos
        metodos.MuestraPersona(p1);
        metodos.ComparaPersonas(p1, p2);
        
    }
}