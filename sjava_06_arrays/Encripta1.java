package sjava_06_arrays;

import java.util.Random;


public class Encripta1 {
    //Reemplaza vocales por números
    public static void VocalesToNumeros(String palabra) {
        char[] letras = palabra.toCharArray();

        StringBuilder sbuilder = new StringBuilder();

        for(char s : letras) {
            char convertedInt = ' ';
            int num = 0;
            if(s == 'a' || s == 'e' || s == 'i' || s == 'o' || s == 'u'){
                //numero random del 1 al 9 codigo ascii
                num = ((int)(Math.random()*(57-48))+48);

                //Parse num from int to char
                convertedInt=(char)num;
                s = convertedInt;
                sbuilder.append(s);
            } else {
                sbuilder.append(s);
            }
        }
        System.out.println(sbuilder);
    }
}