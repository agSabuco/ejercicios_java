public class PersonaController{
    //variables auxiliares
    private String name;
    private int age;
    private String email;
    private int id;

    public void MuestraPersona(Persona pers){
        //asigno los getters a las variables
        name = pers.getNombre();
        age = pers.getEdad();
        email = pers.getEmail();
        id = pers.getId();

        //imprimo variables
        System.out.println(name + " tiene " + age + " a\u00f1os y su email es " + email);
    }

    //metodo que compara edades
    public void ComparaPersonas(Persona pers1, Persona pers2){
        if(pers1.getEdad() > pers2.getEdad()){

            System.out.println(pers1.getNombre() + " (" + pers1.getEdad() + ") es mayor que " +
                                pers2.getNombre() + " (" + pers2.getEdad() + ")" );

        } else if (pers2.getEdad() > pers1.getEdad()){
            System.out.println(pers2.getNombre() + " (" + pers2.getEdad() + ") es mayor que " +
                                pers1.getNombre() + " (" + pers1.getEdad() + ")" );
        } else {
            System.out.println("Tanto " + pers1.getNombre() + " como " + pers2.getNombre() + " tienen la misma edad");
        }
    }
}