package sjava_06c;

import java.util.Scanner;
import java.util.Random;

class PiedraPapelTijeraTest {

    public static void main(String[] args) {
        System.out.println("Este es el juego Piedra, Papel o Tijera! Tu oponente sera CPU. El mejor de 3 gana!");
        int rondas = 0;
        int puntosCpu = 0;
        int puntosUser = 0;

        do {
            int eleccionUser = 0;
            Scanner miInt = new Scanner(System.in); 
            try {                
                System.out.println("Pon el numero de lo que quieras sacar: 1. Piedra , 2. Papel y 3. Tijera ");
                eleccionUser = miInt.nextInt();
            } catch(Exception ex) {
                System.out.println("Entrada invalida vuelve a intentarlo, recuerda: 1. Piedra , 2. Papel y 3. Tijera");                
                eleccionUser = miInt.nextInt();
            }

            //Código que genera un aleatorio de 1 a 3
            Random random = new Random();
            int incognita = random.nextInt(3)+1;

            
            if(incognita == eleccionUser){
                System.out.println("Habeis empatado!");
            } else if(incognita == 1 && eleccionUser == 2){
                System.out.println("La maquina ha sacado " + incognita + ". Piedra" + ". Has ganado!");
                puntosUser++;
            } else if (incognita == 1 && eleccionUser == 3){
                System.out.println("La maquina ha sacado " + incognita + ". Piedra" + ". Has Perdido!");
                puntosCpu++;
            } else if (incognita == 2 && eleccionUser == 1){
                System.out.println("La maquina ha sacado " + incognita + ". Papel" + ". Has perdido!");
                puntosCpu++;
            } else if (incognita == 2 && eleccionUser == 3) {
                System.out.println("La maquina ha sacado " + incognita + ". Papel" + ". Has ganado!");
                puntosUser++;
            } else if (incognita == 3 && eleccionUser == 1){
                System.out.println("La maquina ha sacado " + incognita + ". Tijera" + ". Has ganado!");
                puntosUser++;
            } else if (incognita == 3 && eleccionUser == 2){
                System.out.println("La maquina ha sacado " + incognita + ". Tijera" + ". Has perdido!");
                puntosCpu++;
            }

            //Contador de rondas si no se empata y no se atasca en el catch
            if(incognita != eleccionUser && incognita > 0 && eleccionUser > 0) rondas++;
            
        } while (rondas < 3);
        
        System.out.println("Tu puntuacion es de " + puntosUser + " puntos, y la puntuacion de la CPU es de " + puntosCpu + " puntos");


    }
}