package flota_coches;

import java.util.ArrayList;
//import java.util.Collections;
import java.util.Random;

public class Flota {
    private static ArrayList<Coche> coches = new ArrayList<Coche>();
    private static String[] marcas;
    private static String[] colores;

    
    //genera num de coches aleatorios, tener en cuenta marca, color, kms
    public static void generaCoches(int num) {
        int i = 0;
        
        marcas = new String[] {"Seat","Citroen","Renault","Dacia"};
        colores = new String[] {"Rojo","Blanco","Negro"};

        do {
            //Limpiamos variables
            String colorRandom = " ";
            String marcaRandom = " ";
            int kmRandom = 0;

            //Código que genera un aleatorio de 0 a 3
            Random random = new Random();
            int x = random.nextInt(3);
            colorRandom = colores[x];

            //Código que genera un aleatorio de 0 a 4
            Random random2 = new Random();
            int y = random2.nextInt(4);
            marcaRandom = marcas[y];

            //Código que genera un aleatorio de 1 a 150.000km
            Random random3 = new Random();
            kmRandom = random3.nextInt(150000)+1;

            //Se crea el objeto con nuestras variables random
            Coche objCoche = new Coche(marcaRandom, colorRandom, kmRandom);

            //Añadimos en la coleccion
            coches.add(objCoche);

            //contador de n veces que se haga el bucle
            i++; 

        } while(i<num);

    }

    // x coches en la lista
    // la marca más comun es xxxx y tiene x coches
    // el color más frecuente es el xxxx con x coches
    // x coches tienen más de 100.000 km
    public static void resumen() {
        int numRenault = 0;
        int numDacia = 0;
        int numCitroen = 0;
        int numSeat = 0;

        int numRojos = 0;
        int numBlanco = 0;
        int numNegros = 0;

        int numCochesKm = 0;
        for(Coche currentCoche : coches) {
            if(currentCoche.getMarca() == "Renault"){
                numRenault++;
            } else if (currentCoche.getMarca() == "Dacia") {
                numDacia++;
            } else if (currentCoche.getMarca() == "Citroen") {
                numCitroen++;
            } else if (currentCoche.getMarca() == "Seat") {
                numSeat++;
            }
            if(currentCoche.getColor() == "Rojo"){
                numRojos++;
            } else if (currentCoche.getColor() == "Blanco"){
                numBlanco++;
            } else if (currentCoche.getColor() == "Negro") {
                numNegros++;
            }
            if(currentCoche.getKms() > 100000){
                numCochesKm++;
            }
        }

        int numMax[] = new int[] {numRenault,numDacia,numCitroen,numSeat};
        int numMarcaMasComun = 0;
        for(int i=0;i<numMax.length;i++) {
            numMarcaMasComun = Math.max(numMarcaMasComun, numMax[i]);
        }

        String nombreMarcaMasComun = "";
        if(numMarcaMasComun == numMax[0]){
            nombreMarcaMasComun = "Renault";
        } else if(numMarcaMasComun == numMax[1]) {
            nombreMarcaMasComun = "Dacia";
        } else if(numMarcaMasComun == numMax[2]) {
            nombreMarcaMasComun = "Citroen";
        } else if (numMarcaMasComun == numMax[3]) {
            nombreMarcaMasComun = "Seat";
        }

        int numMaxColor[] = new int[] {numRojos,numBlanco,numNegros};
        int numColorMasComun = 0;
        for(int j=0;j<numMaxColor.length;j++) {
            numColorMasComun = Math.max(numColorMasComun, numMaxColor[j]);
        }

        String colorMasComun = "";
        if(numColorMasComun == numMaxColor[0]){
            colorMasComun = "Rojo";
        } else if(numColorMasComun == numMaxColor[1]) {
            colorMasComun = "Blanco";
        } else if(numColorMasComun == numMaxColor[2]) {
            colorMasComun = "Negro";
        }



        System.out.println(coches.size() + " coches en la lista \n" + 
                            "La marca mas comun es " + nombreMarcaMasComun + " y tiene " + numMarcaMasComun + " coches \n" +
                            "El color mas frecuente es el " + colorMasComun + " con " + numColorMasComun + " coches \n" +
                            numCochesKm + " coches tienen mas de 100.000 km");

    }

    // escribe la lista de coches de la marca solicitada
    public static void consultaMarca(String marca) {
        int numCoches = 0;

        for(Coche c : coches) {
            if(c.getMarca() == marca) {
                numCoches++;
            }
        }
        System.out.println(numCoches + " coches en la lista de la marca " + marca);
    }

    // lista coches de los del color indicado
    public static void consultaColor(String color) {
        //int numCoches = Collections.frequency(coches, color);
        int numCoches = 0;
        for(Coche c1 : coches) {
            if(c1.getColor() == color) {
                numCoches++;
            }
        }
        System.out.println(numCoches + " coches en la lista de color " + color);
    }

    //lista coches con km>= minkm
    public static void consultaKm(int minkm) {
        int cochesRequeridos = 0;

        for(Coche cocheActual : coches){
            if(cocheActual.getKms() >= minkm ){
                cochesRequeridos++;
            }
        }
        
        System.out.println(cochesRequeridos + " coches con km con " + minkm + " kilometros como minimo");
    }

    //
    public static void consultaMultiple(String marca, String color, int minkm) {
        int contBusqueda = 0;
        for(Coche currCoche : coches){
            
            if(currCoche.getMarca() == marca && currCoche.getColor() == color && currCoche.getKms() > minkm){
                System.out.println(currCoche.toString());
                contBusqueda++;
            }
        }

        System.out.println(contBusqueda + " coches encontrados en tu consulta");
    }
}