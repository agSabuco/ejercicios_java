package flota_coches;

public class TestFlota {
    public static void main(String[] args) {
        //invocamos siempre metodo generaCoches
        Flota.generaCoches(1000);

        Flota.resumen();

        Flota.consultaColor("Rojo");

        Flota.consultaMarca("Renault");

        Flota.consultaKm(120000);

        Flota.consultaMultiple("Seat", "Negro", 50000);
    }
}