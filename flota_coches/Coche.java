package flota_coches;

public class Coche {
    private String marca;
    private String color;
    private int kms;


    public Coche(String marca, String color, int kms) {
        this.marca = marca;
        this.color = color;
        this.kms = kms;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    @Override
    public String toString() {
        return "Coche [color=" + color + ", kms=" + kms + ", marca=" + marca + "]";
    }
    
    
}