package sjava_08_fileio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class Lineas {
	public static void main(String[] args) {
		
        File fin= new File("motos.csv");
        File fout = new File("honda.csv");

		try (	FileReader fr = new FileReader(fin);
                BufferedReader br = new BufferedReader(fr);
                FileWriter fw = new FileWriter(fout);
                BufferedWriter bw = new BufferedWriter(fw);
				) {
			String line;
			do {
				line = br.readLine();
				if (line!=null && line.indexOf("HONDA") != -1) {
                    bw.write(line);
                    bw.newLine();
					}
			} while (line!=null);
	
		} catch (Exception e) {
			e.printStackTrace();
		} 

        

	}
}