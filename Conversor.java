public class Conversor {

    public Double convierte(Double valor, String desde, String hacia) {
        double resultado = 0.0;
        
        if( desde.equals("km/h") && hacia.equals("mi/h")){
            resultado = valor / 1.61 / 3600;
        } else if(desde.equals("mi/h") && hacia.equals("km/h")){
            resultado = valor * 1.61 * 3600;
        } else if (desde.equals("km/h") && hacia.equals("m/s")){
            resultado = valor * 1000 / 3600;
        } else if (desde.equals("m/s") && hacia.equals("km/h")) {
            resultado = valor / 1000 * 3600;
        } else if(desde.equals("km/h") && hacia.equals("yd/s")){
            resultado = valor * 1.61 * 1760 / 3600;
        } else if(desde.equals("yd/s") && hacia.equals("km/h")){
            resultado = valor / 1.61 / 1760 * 3600;
        } else if(desde.equals("mi/h") && hacia.equals("yd/s")){
            resultado = valor * 1760 / 3600;
        } else if(desde.equals("yd/s") && hacia.equals("mi/h")){
            resultado = valor / 1760 * 3600;
        } else if(desde.equals("m/s") && hacia.equals("yd/s")) {
            resultado = valor * 1.09;
        } else if (desde.equals("yd/s") && hacia.equals("m/s")) {
            resultado = valor * 0.91;
        } else if (desde.equals("mi/h") && hacia.equals("m/s")){
            resultado = valor * 1609.34 / 3600;
        } else if (desde.equals("m/s") && hacia.equals("mi/h")){
            resultado = valor * 1.0 / 1609.34 / 3600;
        } else {
            throw new IllegalArgumentException("La conversion es invalida");
        }
 
        return resultado;
     }
 
}