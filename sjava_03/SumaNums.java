package sjava_03;


class SumaNums {
    public static void main(String[] args) {
        int total=0;
        int contador = 0;
        for (String s : args){
            int num = Integer.parseInt(s);
            total += num;
            contador++;
        }
        System.out.println("El total es "+total);
        System.out.println("hay " + contador + " numeros");
    }
}
