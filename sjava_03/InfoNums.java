package sjava_03;

public class InfoNums{
    static int contador = 0;
    static int numMax = 0;
    static int numMin = 999999999;
    static int mediaNum = 0;
    static int total=0;
    public static void main(String[] args) {
        for(String s : args){
            int num = Integer.parseInt(s);
            
            numMax = Math.max(numMax, num);
            numMin = Math.min(numMin, num);

            total += num;
            contador++;
        }
        System.out.println(contador + " numeros introducidos \n" +
                            "numero mayor: " + numMax +"\n" +
                            "numero menor: " + numMin +"\n" +
                            "media aritmetica: " + (total/contador));
    }
}