package BicingFold;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Bicing {
	public static void main(String[] args) {
		try {
			Bicing.conecta();
		} catch (Exception e) {
			//TODO: handle exception
		}
		

	}

	public static void conecta(){
		File dest = new File("bicing.json");
		URLConnection conn = null;
		try {
			URL url = new URL("https://api.citybik.es/v2/networks/bicing");
			conn = url.openConnection();
		} catch (MalformedURLException ex) {
			System.out.println("url no valida");
		} catch (IOException e) {
			System.out.println("pagina no responde");
		}
		
		
		try (
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			FileOutputStream fos = new FileOutputStream(dest);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
		) {
			
			int size = 1024;
			byte[] buffer = new byte[size];
			int len;
			while ((len = bis.read(buffer, 0, size)) > -1) {
				bos.write(buffer, 0, len);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

}