import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import com.google.gson.Gson;

public class TestMotoGson {
    public static void main(String[] args) {

        File jsonFile = new File("motos.json");
        StringWriter sw = new StringWriter();
        try (
            FileReader fr = new FileReader(jsonFile); 
            BufferedReader br = new BufferedReader(fr);
        ) {
            int size = 100;
            char[] buffer = new char[size];
            int len;
            while ((len = br.read(buffer, 0, size)) > -1) {
                sw.write(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        MotoGson[] motos = gson.fromJson(sw.toString(), MotoGson[].class);

        for (MotoGson mimoto : motos) {
            System.out.println(mimoto.toString());
        }
    }
}