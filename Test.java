import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        try{
            System.out.println("Este es un programa de conversion de velocidades! \n" +
                                "Para que funcione necesitas poner tus decimales con ',', por ejemplo: 100,00 \n" +
                                "Ademas, las unidades de conversion que admite son: 'km/h', 'm/s', 'mi/h' y 'yd/s'");
            Scanner miDecim = new Scanner(System.in); 
            System.out.println("Pon el decimal al que quieres hacer la conversion:");
            Double valor = miDecim.nextDouble();

            Scanner actualUnit = new Scanner(System.in); 
            System.out.println("Pon la unidad de velocidad que tienes: 'km/h', 'm/s', 'mi/h' o 'yd/s'");
            String desde = actualUnit.nextLine();

            Scanner convertUnit = new Scanner(System.in); 
            System.out.println("Pon la unidad de velocidad a la que quieres convertir: Recuerda! 'km/h', 'm/s', 'mi/h' o 'yd/s'");
            String hacia = convertUnit.nextLine();

            Conversor cnv = new Conversor();
            Double resultado = cnv.convierte(valor, desde, hacia);
            System.out.printf("%.2f %s equivalen a %.2f %s", 
                        valor, desde, resultado, hacia);

        } catch(IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }  
    }
}
