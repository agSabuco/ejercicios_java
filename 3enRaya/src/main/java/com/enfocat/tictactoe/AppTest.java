package com.enfocat.tictactoe;

/*
    El Player1 es el jugador "humano" X, el Player2 es el ordenador O
*/
public class AppTest
{
    public static void main( String[] args )
    {
        Game newGame = new Game();
        newGame.play();
        
    }
}
