import com.google.gson.Gson;


public class TestGson {
    public static void main(String[] args) {
        Gson gson = new Gson();


        // Persona p = new Persona("humbert", 22);
        // gson.toJson(p, System.out);
        
        // String str = gson.toJson(p);
        // System.out.println(str);

        // String js = "{\"nombre\":\"humbert\",\"edad\":22}";

        // Persona p2 = gson.fromJson(js, Persona.class);
        // System.out.println(p2.nombre);


        Persona con1 = new Persona("ana", 21);
        Persona con2 = new Persona("bob", 65);
        Persona con3 = new Persona("joel", 25);
        Persona con4 = new Persona("marti", 33);
        Persona[] contactos = {con1, con2, con3, con4};

        String str = gson.toJson(contactos);
        System.out.println(str);

        Persona[] contactosImportados = gson.fromJson(str, Persona[].class);
        
        for (Persona x : contactosImportados){
            System.out.println(x.toString());
        }

    }
}
