import java.util.HashSet;

public class SetTest {
    public static void main(String[] args) {
        Company c1 = new Company("Oracle", 1977);
        Company c2 = new Company("IBM", 1911);
        Company c3 = new Company("Oracle", 1977);
        Company c4 = new Company("IBM", 1977);
        Company c5 = new Company("IBM", 1965);
        Company c6 = c1;

        HashSet<Company> companies = new HashSet<Company>();

        companies.add(c1);
        companies.add(c2);
        companies.add(c3);
        companies.add(c4);
        companies.add(c5);
        companies.add(c6);

        //Con HashSet nos devolvera tamaño de 5 ya que no hace ninguna comparacion
        System.out.println(companies.size());
    }
}