public class Test2 {
    public static void main(String[] args) {
        Producto p = new Producto("paquete folios");
        Cliente c = new Cliente("Alvaro Garcia");
        Factura f = new Factura(3);

        System.out.println(PintadorTabs.pintaTab(p));
        System.out.println(PintadorTabs.pintaTab(c));
        System.out.println(PintadorTabs.pintaTab(f));
    }
}