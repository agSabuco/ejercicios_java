public class TestMoto {
    public static void main(String[] args) {
        Moto moto1 = new Moto("Honda", "GoldWing", 120, 30000.99);
        Moto moto2 = new Moto("Ducati", "Multistrada", 160, 17000.50);
        Moto moto3 = new Moto("Yamaha", "Tmax", 55, 12000.4);
        Moto moto4 = new Moto("Suzuki", "VStrom 650", 70, 8000.20);
        Moto[] motos = { moto1, moto2, moto3, moto4};

        MotoUtil.informe(motos);

    }
}