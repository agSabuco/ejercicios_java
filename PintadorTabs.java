public class PintadorTabs{

    static String pintaTab(Pintable cosa) {
        if(cosa instanceof Producto) {
            Producto p1 = (Producto) cosa;
            return ((Producto) cosa).getDescription();
        } else if (cosa instanceof Factura){
            Factura p2 = (Factura) cosa;
            return ((Factura) cosa).getNumeroFactura();
        } else if (cosa instanceof Cliente) {
            Cliente p3 = (Cliente) cosa;
            return ((Cliente) cosa).getNombre();
        } else {
            return "error";
        }

      
    }
}