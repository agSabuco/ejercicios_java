<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.javaweb.Pipati" %>
<%@page import="com.enfocat.javaweb.HtmlFactory" %>
<%
    HtmlFactory factory = new HtmlFactory();
    int jugada_usuario = Integer.parseInt(request.getParameter("jugada")); //1
    String respuesta = Pipati.partida(jugada_usuario); //2
%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
    <%= factory.titulo(respuesta) %>
    <a href="index.jsp"><i class="fas fa-redo"></i> Volver a jugar</a>
</body>
</html>